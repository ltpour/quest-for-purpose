//----------------------------------------------------------------------------------------------------
// game_start
//----------------------------------------------------------------------------------------------------
function game_start() 
{
  gQuit.Centre();
  gRestart.Centre();
  gRestore.Centre();
  gSave.Centre();
}

//----------------------------------------------------------------------------------------------------
// repeatedly_execute
//----------------------------------------------------------------------------------------------------
function repeatedly_execute() 
{
}

//----------------------------------------------------------------------------------------------------
// repeatedly_execute_always
//----------------------------------------------------------------------------------------------------
function repeatedly_execute_always() 
{
}

//----------------------------------------------------------------------------------------------------
// on_key_press
//----------------------------------------------------------------------------------------------------
function on_key_press(eKeyCode keycode) 
{
  if (IsGamePaused()) keycode = 0;
  
  // "System Keys"
  if (keycode == eKeyCtrlQ) gQuit.Visible = true; // quit Dialog
  if (keycode == eKeyF5) { // save dialog
    lstSave.FillSaveGameList();
    lstSave.SelectedIndex = 0;
    if(lstSave.SelectedIndex != -1) {
      txtSave.Text = lstSave.Items[lstSave.SelectedIndex];
    }
    gSave.Visible = true;
  }
  if (keycode == eKeyF7) { // restore dialog
    lstRestore.FillSaveGameList();
    lstRestore.SelectedIndex = 0;
    gRestore.Visible = true;
  }
  if (keycode == eKeyF9) gRestart.Visible = true; // restart dialog
  if (keycode == eKeyF12) SaveScreenShot("scrnshot.pcx"); // screenshot
  
  // Debugger Keys
  if (keycode == eKeyCtrlS) Debug(0,0); // Ctrl-S, give all inventory
  if (keycode == eKeyCtrlV) Debug(1,0); // Ctrl-V, version
  if (keycode == eKeyCtrlA) Debug(2,0); // Ctrl-A, show walkable areas
  if (keycode == eKeyCtrlX) Debug(3,0); // Ctrl-X, teleport to room
}

//----------------------------------------------------------------------------------------------------
// on_mouse_click
//----------------------------------------------------------------------------------------------------
function on_mouse_click(MouseButton button)
{
	// all mouse clicks are handled in TwoClickHandler.asc!
}

//----------------------------------------------------------------------------------------------------
// on_event
//----------------------------------------------------------------------------------------------------
function on_event (EventType event, int data) 
{
}

//----------------------------------------------------------------------------------------------------
// unhandled_event
//----------------------------------------------------------------------------------------------------
function unhandled_event (int what, int type) 
{
	if (what == 1) // Unhandled events for HOTSPOTS
	{
		if (type == 1) // look
		{
			player.Say("I see nothing special about it.");
		}
		if (type == 2) // interact
		{
			player.Say("I can't do anything with it.");
		}
		if (type == 3) // use inv on
		{
			player.Say("That won't do anything.");
		}
	}
	if (what == 2) // Unhandled events for OBJECTS
	{
		if (type == 0) // look
		{
			player.Say("Looks alright.");
		}
		if (type == 1) // interact
		{
			player.Say("I don't want to have it.");
		}
		if (type == 3) // use inv on
		{
			player.Say("Umm . . . no.");
		}
	}
	if (what == 3) // Unhandled events for CHARACTERS
	{
		if (type == 0) // look
		{
			player.Say("Hm.");
		}
		if (type == 1) // interact
		{
			player.Say("Got nothing to say.");
		}
		if (type == 3) // use inv on
		{
			player.Say("I don't think she'd want that.");
		}
	}
	if (what == 5) // Unhandled events for INVENTORY ITEMS
	{
		if (type == 0) // look
		{
			player.Say("It's just some junk in my inventory.");
		}
		if (type == 1) // interact
		{
			player.Say("Er, no?");
		}
		if (type == 3) // use inv on
		{
      int r = Random(2);
      if (r == 0) {
        player.Say("That doesn't really make a whole lot of sense.");
      } else if (r == 1) {
        player.Say("Umm . . .");
      } else {
        player.Say("I don't really know what to do with them.");
      }
		}
	}
}

//----------------------------------------------------------------------------------------------------
// dialog_request
//----------------------------------------------------------------------------------------------------
function dialog_request(int param) 
{
}

//----------------------------------------------------------------------------------------------------
// gInventoryBar
//----------------------------------------------------------------------------------------------------
function btnInvScrollLeft_OnClick(GUIControl *control, MouseButton button)
{
	InventoryWindow1.ScrollDown();
}

function btnInvScrollRight_OnClick(GUIControl *control, MouseButton button)
{
	InventoryWindow1.ScrollUp();
}

function btnQuitDialog_OnClick(GUIControl *control, MouseButton button)
{
	gQuit.Visible = true;
}

//----------------------------------------------------------------------------------------------------
// gQuit
//----------------------------------------------------------------------------------------------------
function btnQuit_OnClick(GUIControl *control, MouseButton button)
{
  QuitGame(0);
}

function btnCancelQuit_OnClick(GUIControl *control, MouseButton button)
{
  gQuit.Visible = false;
}

//----------------------------------------------------------------------------------------------------
// gRestart
//----------------------------------------------------------------------------------------------------
function btnRestart_OnClick(GUIControl *control, MouseButton button)
{
  RestartGame();
}

function btnCancelRestart_OnClick(GUIControl *control, MouseButton button)
{
  gRestart.Visible = false;
}

//----------------------------------------------------------------------------------------------------
// gSave
//----------------------------------------------------------------------------------------------------
function btnSave_OnClick(GUIControl *control, MouseButton button)
{
  int totalSaves = lstSave.ItemCount;
  String saveName = txtSave.Text;
  
  // make sure that a name has been entered
  if(saveName == "") {
    Display("Please enter a name for your saved game.");
    return;
  }
  
  // if no saves exist, save to slot 1
  if(lstSave.SelectedIndex == -1) {
    gSave.Visible = false;
    SaveGameSlot(1, saveName);
  } else {
    // if name matches existing save, overwrite it
    int checkSave = 0;
    while(checkSave != totalSaves) {
      if(saveName == lstSave.Items[checkSave]) {
        gSave.Visible = false;
        SaveGameSlot(savegameindex[checkSave], saveName);
        return;
      }
      checkSave++;
    }
    // no match, save to a new slot
    if(totalSaves < 20) {
      gSave.Visible = false;
      SaveGameSlot(totalSaves+1, saveName);
    } else {
      Display("The maximum number of saved games has been reached; overwrite or delete some old ones.");
    }
  }
}

function btnDeleteSave_OnClick(GUIControl *control, MouseButton button)
{
  if(lstSave.SelectedIndex == -1) {
    Display("There are no saved games to delete.");    
  } else {
    DeleteSaveSlot(savegameindex[lstSave.SelectedIndex]);
    lstSave.FillSaveGameList();
  }
}

function btnCancelSave_OnClick(GUIControl *control, MouseButton button)
{
  gSave.Visible = false;
}

function txtSave_OnActivate(GUIControl *control)
{
  txtSave.Text = lstSave.Items[lstSave.SelectedIndex];
}

function lstSave_OnSelectionChanged(GUIControl *control)
{
  txtSave.Text = lstSave.Items[lstSave.SelectedIndex];
}

//----------------------------------------------------------------------------------------------------
// gRestore
//----------------------------------------------------------------------------------------------------
function btnRestore_OnClick(GUIControl *control, MouseButton button)
{
  if(lstRestore.SelectedIndex == -1) {
    Display("There is no saved game to load.");
  } else {
    RestoreGameSlot(savegameindex[lstRestore.SelectedIndex]);
    gRestore.Visible = false;
  }
}

function btnCancelRestore_OnClick(GUIControl *control, MouseButton button)
{
  gRestore.Visible = false;
}

//----------------------------------------------------------------------------------------------------
// Characters
//----------------------------------------------------------------------------------------------------

function cPlayer_Look()
{
  int r = Random(2);
  if (r == 0) {
    player.Say("It is I.");
  } else if (r == 1) {
    player.Say("So this is who I am.");
  } else {
    player.Say("I'd rather look at things other than myself.");
  }
}

function cPlayer_Interact()
{
  int r = Random(2);
  if (r == 0) {
    player.Say("I'd prefer not to.");
  } else if (r == 1) {
    player.Say("Maybe later.");
  } else {
    player.Say("No.");
  }
}

function cGirl_Look()
{
  int r = Random(2);
  if (r == 0) {
    player.Say("It's a girl.");
  } else if (r == 1) {
    player.Say("Looks like a female.");
  } else {
    player.Say("She's pretty.");
  }
}

function cGirl_Interact()
{
  int r = Random(2);
  if (r == 0) {
    player.Say("That would be disrespectful.");
  } else if (r == 1) {
    player.Say("I'd better not.");
  } else {
    player.Say("Umm . . . no.");
  }
}

//----------------------------------------------------------------------------------------------------
// Inventory Items
//----------------------------------------------------------------------------------------------------

function iBacon_Look()
{
	player.Say("It's meat.");
}

function iCup_Look()
{
	player.Say("It's a cup.");
}

function iBook_Look()
{
  player.Say("It's a book.");
}

function iBottle_Look()
{
  player.Say("It's a bottle.");
}

function iPipes_Look()
{
	player.Say("I just started to learn how to play them.");
}

function iPipes_Interact()
{
	player.Say("Pfeeeeeeeehhhhppppphrrrrrfff...");
}

function iDoor_Look()
{
  player.Say("It's a door. It's locked.");  
}

function iWindow_Look()
{
  player.Say("It's a window. It's closed.");
}

function iTable_Look()
{
  player.Say("It's a table.");
}

function iCigarettes_Look()
{
  player.Say("It's a pack of cigarettes.");
}

function iCigarettes_Interact()
{
  player.Say("I think I'll save them for later.");
}

function iBucket_Look()
{
  player.Say("It's a bucket.");
}

