# Quest for Purpose #

A short, experimental point'n'click game made for the Meta Game Jam in 2018 with Adventure Game Studio 3.4.1.

The fonts included are ROTORcap Neue by David Lindecrantz and David Device by David Roberge.
